# Data Bazaar 2023

## Title
GitLab: Collaborating within and across UW Administrative Units

## Proposal
Administrative staff across UW—Madison are using a variety of tools to do similar tasks, from project management to data analysis and visualization. GitLab allows staff members to use their own preferred tools and still collaborate on everyday tasks. Our poster, created by Policy & Planning Analysts from the School of Education, Wisconsin School of Business, and the College of Letters & Science, will illustrate how we use GitLab to meet challenges related to:
- Project management; categorize and prioritize multiple projects, set timelines, assign tasks, track time, manage and monitor communication
- Security; ensure that users access only appropriate files via single sign on
- Proliferation of analytic tools; share any type of scripted code such as SQL, R, Python, and SAS
- Version control; track and record file changes for individuals and collaborative groups
- Publish and share; report analytic results on a secure platform

Our aim is to overcome the perception that GitLab is a tool only useful for software development. We will provide information on how to get started with GitLab, concrete examples of collaborative work, and challenges. Administrative staff are welcome to connect with us to share projects that are of broad interest to administrative units. 

## Project Contributors
Sara M. Alva Lizarraga, Policy & Planning Analyst, School of Education  
Caleb Basu, Policy & Planning Analyst, Wisconsin School of Business  
Nate Kelty, Policy & Planning Analyst, Wisconsin School of Business  
Susan M. McMillan, Policy & Planning Analyst, Academic Information Management, College of Letters & Science  
Wyl Schuth, Policy & Planning Analyst, Academic Information Management, College of Letters & Science  
Michael Zenz, Policy & Planning Analyst, Academic Information Management, College of Letters & Science  


