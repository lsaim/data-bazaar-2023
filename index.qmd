---
title: "GitLab: Webpage Reporting"
title-block-banner: "#8a8c8e"
# author:
#   - name: Sara M. Alva Lizarraga
#     affiliations: 
#       - UW--Madison School of Education, Dean's Office
#     corresponding: false
#   - name: Caleb Basu
#     affiliations:
#       - UW--Madison Wisconsin School of Business
#     corresponding: false
#   - name: Nate Kelty
#     affiliations:
#       - UW--Madison Wisconsin School of Business
#     corresponding: false
#   - name: Susan M. McMillan (smcmillan2@wisc.edu)
#     email: smcmillan2@wisc.edu
#     affiliations:
#       - UW--Madison College of Letters & Science, Academic Information Management
#     corresponding: true
#   - name: Wyl Schuth
#     affiliations:
#       - UW--Madison College of Letters & Science, Academic Information Management
#     corresponding: false
#   - name: Michael Zenz
#     affiliations:
#       - UW--Madison College of Letters & Science, Academic Information Management
#     corresponding: false
format: 
  html:
    theme: lux-light
    number-sections: true
    number-depth: 2
    embed-resources: true
    page-layout: full
#    linkcolor: "#a51d36"
---

```{r}
#| label: setup 
#| include: false
#| echo: false
#| message: false
#| warning: false

# Install required packages if needed
if (!require("tidyverse")) install.packages("tidyverse")
if (!require("maps")) install.packages("maps")
if (!require("mapproj")) install.packages("mapproj")
if (!require("GGally")) install.packages("GGally")
if (!require("posterdown")) install.packages("posterdown")
if (!require("iheiddown")) install.packages("iheiddown")
if (!require("kableExtra")) install.packages("kableExtra")
if (!require("ggpmisc")) install.packages("ggpmisc")
if (!require("DT")) install.packages("DT")

# Load Packages
library(AIMutils)
library(tidyverse)
library(maps)
library(mapproj)
library(GGally)
library(posterdown)  # basic poster templates
library(iheiddown)   # additional poster templates
library(kableExtra)

source("~/data-bazaar-2023/Code/WI_HS_Data_Exploration.R")


```


:::{.panel-tabset}

## Introduction {.unnumbered}

UW--Madison staff members have many options for tools they use to complete similar tasks and this can make collaboration difficult. This report provides an example of how we use GitLab to generate reports and share analytic results. 

> For an example exploratory data project, we used publicly available information to investigate the relationship between poverty estimates and UW--Madison admit rates for Wisconsin public high schools. 

To indicate poverty, we used an **income-to-poverty ratio** (IPR) estimate: "the percentage of family income that is above or below the federal poverty threshold set for the family’s size and structure." A value of 100 indicates the family is at the federal poverty threshold; *higher* values indicate *less* poverty. The IPR estimates are available from the [U.S. Census Bureau](https://www.census.gov/programs-surveys/saipe.html).

The **admit rate** is the "Percentage of applicants offered admission." The UW--Madison admit rates are from this [UW Institutional data visualization tool](https://tableau.wisconsin.edu/views/PipelineofWIPublicHighSchoolGraduatestoUW-Madison/HomePage).  

The data were originally available at the level of WI Public High Schools. We also aggregated to average values for WI counties. 

The report was created using [Quarto](https://docs.posit.co/resources/install-quarto/#download-and-install-quarto-using-deb-file), and the results are made available as a webpage through GitLab. We used R code to make the graphics, but we could have used Python, or a mix of R and Python. The data and report files are available at this GitLab repository [Data Bazaar 2023](https://git.doit.wisc.edu/lsaim/data-bazaar-2023). A [web version of our Data Bazaar poster](https://lsaim.pages.doit.wisc.edu/data-bazaar-2023/PosterTemplate/collab_posterdown_html.html) is also available.


**Authors:**

Sara M. Alva Lizarraga, UW--Madison School of Education, Dean's Office  
Caleb Basu, UW--Madison Wisconsin School of Business  
Nate Kelty, UW--Madison Wisconsin School of Business  
[Susan McMillan (contact)](mailto:smcmillan2@wisc.edu){.email}, UW--Madison College of Letters & Science, Academic Information Management  
Wyl Schuth, UW--Madison College of Letters & Science, Academic Information Management  
Michael Zenz, UW--Madison College of Letters & Science, Academic Information Management  

(Report last updated on `r format(Sys.time(), format = "%B %d, %Y  %I:%M %p")`).

\ 


## Tabular Report

Table 1 provides the number of public high schools in each WI county, the average UW--Madison admit rate for those schools for the 2021-22 academic year, and the average income-to-poverty ratio estimate for the public high schools in each county. The county-level admit rates may differ from institutional results because our data file is constrained by missing IPR estimate data. 

The table is searchable and sortable so users can take advantage of features not available in static report files.

```{r}
#| echo: false
#| warning: false
#| message: false


DT::datatable(table, class = 'cell-border stripe',
              rownames = FALSE,
              options = list(
                columnDefs = list(list(className = 'dt-right',
                                       targets = 1:3))
              ),
              caption = "Table 1: UW--Madison Average Admit Rate and IPR Estimate by County")


```

\ 
\ 

## Graphic Report

To explore the potential relationship between the UW--Madison admit rate and poverty, we first show side-by-side WI maps of the average admit rate and average income-to-poverty ratio estimate by county. 

@fig-maps indicates that there is no clear and obvious relationship between admit rates and poverty levels, at least at the county level.


```{r}
#| label: fig-maps
#| fig-cap: "UW--Madison Average Admit Rate (2021-22) and Income-to-Poverty Ratio by County"
#| fig-subcap:
#|   - "UW Madison Average Admit Rate (2021-22)"
#|   - "Average Income-to-Poverty Estimate"
#| layout-ncol: 2
#| column: page
#| echo: false
#| warning: false
#| message: false

wi_admit_rate_county_map <- pipeline_poverty %>%
  group_by(., CountyName) %>%
  summarize(., AdmitRate = round(mean(School_Admit_Rate,
                                      na.rm = TRUE),3)) %>%
  full_join(., y = wi_counties,
            by = c("CountyName" = "subregion")) %>%
  ggplot(.,aes(x = long, y = lat,
               group = group,
               fill = AdmitRate)) +
  geom_polygon(color = "gray35", linewidth = 0.1) +
  labs(title = "Average UW--Madison Admit Rate for WI Public High Schools by County",
       subtitle = "(darker color indicates higher admit rate, dark gray indicates missing data)",
       fill = "") +
  scale_fill_binned(high = "#C90814", low = "#Eedcdd",
                    na.value = "gray70",
                    n.breaks = 6) +
  coord_map(projection = "albers", lat0 = 40, lat1 = 45) +
  map_theme_A +
  theme(legend.key.width = unit(1, 'cm'))
wi_admit_rate_county_map



wi_ipr_county_map <- pipeline_poverty %>%
  group_by(., CountyName) %>%
  summarize(., IPR_EST = mean(IPR_EST, na.rm = TRUE)) %>%
  full_join(., y = wi_counties,
            by = c("CountyName" = "subregion")) %>%
  ggplot(.,aes(x = long, y = lat,
               group = group,
               fill = IPR_EST)) +
  geom_polygon(color = "gray35", linewidth = 0.1) +
  labs(title = "Average Family IPR Estimate in WI Public High Schools",
       subtitle = "(darker color indicates more poverty, dark gray indicates missing data)",
       fill = "") +
  scale_fill_binned(high = "#D9E4FF", low = "#054FB9",
                    na.value = "gray70") +
  coord_map(projection = "albers", lat0 = 40, lat1 = 45) +
  map_theme_A +
  theme(legend.key.width = unit(1, 'cm'))
wi_ipr_county_map


```

\ 
\ 

A scatterplot with a linear regression line supports the conclusion that there is no (linear) statistical relationship between the UW--Madison admit rates and the income-to-poverty ratio in WI public high schools.


```{r}
#| label: fig-admit_ipr_plot
#| fig-cap: "UW--Madison Average Admit Rate (2021-22) and Income-to-Poverty Ratio by High School"
#| echo: false
#| warning: false
#| message: false


ggplot(data = pipeline_poverty, aes(
  x = IPR_EST,
  y = School_Admit_Rate )) +
  geom_point(color = "#80391e", alpha = .7) +
  geom_smooth(method = "lm", formula = y ~ x) +
  ggpmisc::stat_poly_eq(aes(label = paste(..eq.label.., sep = "~~~")),
               label.x.npc = "right", label.y.npc = 0.95,
               eq.with.lhs = "italic(hat(y))~`=`~",
               eq.x.rhs = "~italic(x)",
               formula = formula1,
               parse = TRUE,
               size = 3.5, color = "gray40") +
  ggpmisc::stat_poly_eq(aes(label = paste(..rr.label.., sep = "~~~")),
               label.x.npc = "right", label.y.npc = .90,
               formula = formula1, parse = TRUE,
               size = 3.5, color = "gray40") +
  labs(title = "WI Public H.S. UW--Madison Admit Rate and Income-to-Poverty Ratio",
       x = "Income-to-Poverty Ratio",
       y = "UW--Madison Admit Rate") +
  # annotate("text", x = .65, y = .65, color = "gray40",
  #          label = paste("Pearson's r = ", corr_app_rate_frpl, sep = "")) +
  plot_theme_B


```

\ 
\ 


:::

